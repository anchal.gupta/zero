.. include:: /defs.txt

Examples
========

Basic circuits
--------------

Some basic circuit example scripts are provided in the |Zero| source directory, or in the
`development repository`_. These scripts can be run directly as long as |Zero| is installed and
available.

LISO
----

.. toctree::
    :maxdepth: 2

    liso-input

.. _development repository: https://git.ligo.org/sean-leavey/zero/tree/master/examples/native
